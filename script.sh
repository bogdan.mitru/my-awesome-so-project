#!/bin/bash
# verifica daca a primit exact 1 argument
if [[ $# -eq 1 ]]; then
    #verifica daca argumentul este alfanumeric
    if [[ $1 =~ ^[[:alnum:]]$ ]]; then
        count=0
        # citeste linii de la standard input pana la final
        while read -r line; do
            
            #conditie pentrua fi pozitie corecta
            #if [[ $line =~ ^[[:space:]]*[A-Z].*[[:alnum:] ,.!?]+[.!?]$ && ! $line =~ ,\ *and ]];
            if [[ $line =~ ^[[:space:]]*[A-Z].*[a-zA-Z0-9\ ,.!?][a-zA-Z0-9\ ,.!?]*[.!?]$ && ! $line =~ ,\ *and ]]; then
                ((count++))
            fi
            
        done
        echo "numar linii valide: $count"

    else
        echo "Eroare: trebuie doar un singur caracter alfanumeric"
    fi
else
    echo "Eroare: Trebuie un singur argument"
fi
