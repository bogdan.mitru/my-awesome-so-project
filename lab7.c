#include "big_header.h"
#include <dirent.h>
#include <stdbool.h>

bool is_bmp_file(char *filename) {
	char *extension = strstr(filename, ".bmp");
	if(!extension) {
		return false;
	}
	return true;
}


int main(int argc, char *argv[]) {
	check_arguments(argc);
	open_file_stdout();
	DIR *openDir = opendir(argv[1]);
	if(!openDir) {
		printf("Directory could not be opened.\n");
		closedir(openDir);
	}
	
	struct dirent *entry;
	while((entry = readdir(openDir)) != NULL) {
		struct stat fileStat;
		char path[256];
		snprintf(path, sizeof(path), "%s/%s", argv[1], entry->d_name);
		//printf("file name: %s\n", entry->d_name);
		if(stat(path, &fileStat) == 0) {
			if(S_ISREG(fileStat.st_mode) && !is_bmp_file(entry->d_name)) {
				reg_info(path);
				printf("\n");
			} else if(is_bmp_file(entry->d_name) == true) {
				bmp_info(path);
				printf("\n");
			} else if(S_ISLNK(fileStat.st_mode)) {
				link_info(path);
			} else if(S_ISDIR(fileStat.st_mode)) {
				directory_info(path);
			}
		} else {
			printf("smth wrong took place here!\n");
		}
	}
	return 0;
}
