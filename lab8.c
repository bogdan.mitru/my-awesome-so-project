#include "big_header2.h"
#include <sys/wait.h>
/*
bool is_bmp_file(char *filename) {
	char *extension = strstr(filename, ".bmp");
	if(!extension) {
		return false;
	}
	return true;
}
*/
int main(int argc, char *argv[]) {
	check_arguments(argc);
	//open_file_stdout();
	DIR *openDir = opendir(argv[1]);
	if(!openDir) {
		printf("Directory could not be opened.\n");
		closedir(openDir);
	}
	
	
	pid_t pid;
	int status;
	
	struct dirent *entry;
	while((entry = readdir(openDir)) != NULL) {
		if(entry->d_name[0] != '.') {
			pid = fork();
			if(pid == 0) {
				int num_lines = 0;
				//printf("child process!\n");
				char *file_name = entry->d_name;
				printf("%s\n", file_name); //cea mai importanta parte din tot proiectul
				char *f_name = make_statistica_name(file_name);
				printf("%s\n", f_name);
				open_file_stdout_new(f_name, argv[2]);
				//printf("%s\n", file_name);
				file_info1(argv[1], entry);
				printf("\n\n");
				break;
			} else if(pid > 0) {
				//printf("parents process!\n");
				waitpid(pid, &status, 0);
				if(WIFEXITED(status)) {
					printf("Child process with PID %d exited with code %d\n", pid, WIFEXITED(status));
				} else {
					printf("Child process with PID %d exited wrong\n", pid);
				}
			} else {
				printf("fork failed!\n");
				return 1;
			}
			//file_info1(argv[1], entry);
		}
		
	}
	
	closedir(openDir);
	return 0;
}
