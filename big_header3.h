#ifndef BIG_HEADER3_H_
#define BIG_HEADER3_H_

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <dirent.h>

typedef struct {
	unsigned char red, green, blue;	
} Pixel;

bool is_bmp_file(char *filename) {
	char *extension = strstr(filename, ".bmp");
	if(!extension) {
		return false;
	}
	return true;
}

void check_arguments(int argc) {
	if(argc != 4) {
		printf("Wrong arguments!\n");
		exit(0);
	}
}

int file_opener(char *argv) {
	int fileopener = open(argv, O_RDONLY);
	if(fileopener == -1) {
		printf("Could not open the file !\n");
		exit(0);
	}
	return fileopener;
}

void print_file_name(char *argv) {
	printf("file name: %s\n", argv);
}

void print_size(int fileopener) {
	if(lseek(fileopener, 2, SEEK_SET) == -1) {
		printf("Lseek failed!\n");
		close(fileopener);
		exit(0);
	}
	int size;
	
	ssize_t bytesRead = read(fileopener, &size, sizeof(int));
	if(bytesRead == -1) {
		printf("read failed!\n");
		exit(0);
	}
	printf("size:%d\n", size);
}

void print_linked_file_size(int fileopener) {
	
}

int calc_size(int fileopener) {
	if(lseek(fileopener, 18, SEEK_SET) == -1) {
		printf("Lseek failed!\n");
		close(fileopener);
		exit(0);
	}
	
	int width;
	ssize_t bytesRead = read(fileopener, &width, sizeof(int));
	if(bytesRead == -1) {
		printf("read failed!\n");
		exit(0);
	} else if(bytesRead == 0) {
		printf("end of file!\n");
		exit(0);
	}
	
	
	if(lseek(fileopener, 22, SEEK_SET) == -1) {
		printf("Lseek failed!\n");
		close(fileopener);
		exit(0);
	}
	
	int height;
	bytesRead = read(fileopener, &height, sizeof(int));
	if(bytesRead == -1) {
		printf("read failed!\n");
		exit(0);
	} else if(bytesRead == 0) {
		printf("end of file!\n");
		exit(0);
	}
	return width * height;
}

void print_width_height(int fileopener) {
	if(lseek(fileopener, 18, SEEK_SET) == -1) {
		printf("Lseek failed!\n");
		close(fileopener);
		exit(0);
	}
	
	int width;
	ssize_t bytesRead = read(fileopener, &width, sizeof(int));
	if(bytesRead == -1) {
		printf("read failed!\n");
		exit(0);
	} else if(bytesRead == 0) {
		printf("end of file!\n");
		exit(0);
	}
	
	
	if(lseek(fileopener, 22, SEEK_SET) == -1) {
		printf("Lseek failed!\n");
		close(fileopener);
		exit(0);
	}
	
	int height;
	bytesRead = read(fileopener, &height, sizeof(int));
	if(bytesRead == -1) {
		printf("read failed!\n");
		exit(0);
	} else if(bytesRead == 0) {
		printf("end of file!\n");
		exit(0);
	}
	
	printf("width:%d\n", width);
	printf("height:%d\n", height);
}

void print_owner_id(struct stat filestats) {
	printf("owner's id:%d\n", filestats.st_uid);
}

void print_last_modification(struct stat filestats) {
	printf("last modification:%s", ctime(&filestats.st_mtim));
}

void print_perms(struct stat filestats) {
	printf("user perms:");
	if(filestats.st_mode & S_IRUSR) {
		printf("read ");
	}
	if(filestats.st_mode & S_IWUSR) {
		printf("write ");
	}
	if(filestats.st_mode & S_IXUSR) {
		printf("execute ");
	}
	
	printf("\n");
	
	printf("group perms:");
	if(filestats.st_mode & S_IRGRP) {
		printf("read ");
	}
	if(filestats.st_mode & S_IWGRP) {
		printf("write ");
	}
	if(filestats.st_mode & S_IXGRP) {
		printf("execute ");
	}
	
	printf("\n");
	
	printf("other perms:");
	if(filestats.st_mode & S_IROTH) {
		printf("read ");
	}
	if(filestats.st_mode & S_IROTH) {
		printf("write ");
	}
	if(filestats.st_mode & S_IXOTH) {
		printf("execute ");
	}
	printf("\n");
}

void print_links(struct stat filestats) {
	printf("Number of links:%d\n", filestats.st_nlink);
}

void open_file_stdout() {
	int outputFile = open("statistica.txt", O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
	
	if(outputFile == -1) {
		printf("Could not create/open statistica for writing!\n");
		exit(0);
	}
	
	dup2(outputFile, STDOUT_FILENO);
	close(outputFile);
}

void open_file_stdout_new(char *filename, char *output_dir) {
	strcat(output_dir, "/");
	char *complete_path = output_dir;
	strcat(complete_path, filename);
	int outputFile = open(complete_path, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
	
	if(outputFile == -1) {
		printf("Could not create/open statistica for writing!\n");
		exit(0);
	}
	
	dup2(outputFile, STDOUT_FILENO);
	close(outputFile);
}

void bmp_info(char *filename) {
	int fileopener = file_opener(filename);
	print_file_name(filename);
	print_width_height(fileopener);
	print_size(fileopener);
	struct stat filestats;
	if(stat(filename, &filestats) == -1) {
		printf("Could not get file information.\n");
		exit(0);
	}
	
	print_owner_id(filestats);
	
	print_last_modification(filestats);
	
	print_perms(filestats);
	
	print_links(filestats);
	
	close(fileopener);
}

void reg_info(char *filename) {
	int fileopener = file_opener(filename);
	print_file_name(filename);
	struct stat filestats;
	if(stat(filename, &filestats) == -1) {
		printf("Could not get file information.\n");
		exit(0);
	}
	
	print_size(fileopener);
	
	print_owner_id(filestats);
	
	print_last_modification(filestats);
	
	print_perms(filestats);
	
	print_links(filestats);
	
	close(fileopener);
}

void link_info(char *filename) {
	int fileopener = file_opener(filename);
	print_file_name(filename);
	struct stat filestats;
	if(stat(filename, &filestats) == -1) {
		printf("Could not get file information.\n");
		exit(0);
	}
	print_size(fileopener);
	print_owner_id(filestats);
	print_last_modification(filestats);
	print_perms(filestats);
	print_links(filestats);
	close(fileopener);
}

void directory_info(char *filename) {
	int fileopener = file_opener(filename);
	print_file_name(filename);
	struct stat filestats;
	if(stat(filename, &filestats) == -1) {
		printf("Could not get file information.\n");
		exit(0);
	}
	
	print_owner_id(filestats);
	
	print_perms(filestats);
	
	close(fileopener);
}

void file_info(char *argv, struct dirent *entry) {
	struct stat fileStat;
	char path[256];
	snprintf(path, sizeof(path), "%s/%s", argv, entry->d_name);
	if(stat(path, &fileStat) == 0) {
		if(S_ISREG(fileStat.st_mode) && !is_bmp_file(entry->d_name)) {
			reg_info(path);
			printf("\n");
		} else if(is_bmp_file(entry->d_name) == true) {
			bmp_info(path);
			printf("\n");
		} else if(S_ISLNK(fileStat.st_mode)) {
			link_info(path);
		} else if(S_ISDIR(fileStat.st_mode)) {
			directory_info(path);
		}
	} else {
			printf("smth wrong took place here!\n");
	}
}

void file_info1(char *argv, struct dirent *entry) {
	struct stat fileStat;
	char path[256];
	snprintf(path, sizeof(path), "%s/%s", argv, entry->d_name);
	//vezi dc nu intra in interogare stat cand dai de link
	if(stat(path, &fileStat) == 0) {
		if(S_ISLNK(fileStat.st_mode)) {
			link_info(path);
		} else if(S_ISREG(fileStat.st_mode) && !is_bmp_file(entry->d_name)) {
			reg_info(path);
			pid_t pid = fork();
			if(pid == 0) {
				printf("reach here in reg\n");
				char command[100];
				snprintf(command, sizeof(command), "/bin/bash -c 'bash script.sh %s'", entry->d_name);
				FILE *pipe = popen(command, "r");
				if(!pipe) {
					printf("Big error\n");
				}
			} else if(pid > 0) {
				waitpid(pid, NULL, 0);
			} else {
				printf("fork failed!\n");
				exit(0);
			}
			//printf("\n");
		} else if(is_bmp_file(entry->d_name) == true) {
			bmp_info(path);
			pid_t pid = fork();
			if(pid == 0) {
				printf("reach here0\n");
				change_colors(entry->d_name);
				printf("reach here2\n");
			}
			//printf("\n");
		} else if(S_ISDIR(fileStat.st_mode)) {
			directory_info(path);
		}
	} else {
		printf("smth wrong took place here!\n");
	}
}

char* make_statistica_name(char* entry_name) {
	bool isdir = true;
	char *saved_name;
	if (strlen(entry_name) < 299) {
		for (int i = 0; i < strlen(entry_name); i++) {
			if (entry_name[i] == '.') {
				strncpy(saved_name, entry_name, i);
				isdir = false;
				
				break;
			}
		}
		if(isdir == true) {
			strcpy(saved_name, entry_name);
		}
		strcat(saved_name, "_statistica.txt");	
		
	}
	
	
	
	else {
		printf("file name too large.\n");
	}
	
	return saved_name;
}

void change_bmp_colors(char *filename) {
	int fileopener = file_opener(filename);
	
	int bmp_size = calc_size(fileopener);
	
	Pixel *pixel_table = (Pixel*)malloc(sizeof(Pixel) * bmp_size);
	
	const double var1 = 0.299, var2 =  0.587, var3 = 0.114;
	
	read(fileopener, pixel_table, bmp_size * sizeof(Pixel));
	
	for(int i = 0; i < bmp_size; i++) {
		unsigned char gray = var1 * pixel_table[i].red + var2 * pixel_table[i].green + var3 * pixel_table[i].blue;
		pixel_table[i].red = gray;
		pixel_table[i].green = gray;
		pixel_table[i].blue = gray;
		//printf("%d\n", gray);
	}
	
	lseek(fileopener, 54, SEEK_SET);
	
	write(fileopener, pixel_table, bmp_size * sizeof(Pixel));
	
	free(pixel_table);
	printf("reach here1\n");
	close(fileopener);
}

void change_colors(char *filename) {
	int fileopener = file_opener(filename);
	
	int bmp_size = calc_size(fileopener);
	
	//Pixel *pixel_table = (Pixel*)malloc(sizeof(Pixel) * bmp_size);
	
	const double var1 = 0.299, var2 =  0.587, var3 = 0.114;
	
	for(int i = 0; i < bmp_size; i++) {
		unsigned char red, green, blue;
		lseek(fileopener, 54, SEEK_SET);
		read(fileopener, &red, sizeof(unsigned char));
		lseek(fileopener, 55, SEEK_SET);
		read(fileopener, &green, sizeof(unsigned char));
		lseek(fileopener, 56, SEEK_SET);
		read(fileopener, &blue, sizeof(unsigned char));
		
		unsigned char gray = var1 * red + var2 * green + var3 * blue;
		red = gray;
		green = gray;
		blue = gray;
		
		lseek(fileopener, 54, SEEK_SET);
		write(fileopener, &red, sizeof(unsigned char));
		lseek(fileopener, 55, SEEK_SET);
		write(fileopener, &green, sizeof(unsigned char));
		lseek(fileopener, 56, SEEK_SET);
		write(fileopener, &blue, sizeof(unsigned char));
		
		
	}
	
	//lseek(fileopener, 54, SEEK_SET);
	
	//write(fileopener, pixel_table, bmp_size * sizeof(Pixel));
	
	//free(pixel_table);
	printf("reach here1\n");
	close(fileopener);
}

#endif
